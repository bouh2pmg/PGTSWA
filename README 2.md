# PGT4SWA
This Repository contains all informations about PGT4SWA units. 

## Administration
- [ ] Incomming

## CompteRendu
- [ ] Incomming

## Documents
- [ ] Incomming

## Modules
All informations about units. This folder contains agenda for entire year ('Planning PGT4.xlsx').

All units are defined on 'Planning et fiches modules T7.xlsx' and 'Planning et fiches modules T8.xlsx' files.

Credits are explained on 'Décompte crédits PGTSWA.xlsx' file.

In each unit folder, you can find mardown file describ entire unit. 

## Presentations
Incomming
