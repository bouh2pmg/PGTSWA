# Introdution
The aim of this request of proposal (RFP) is to present the requirements for the **NAME_OF_PROJECT** project.

# Company description
**NAME_OF_COMPANY** is a French multinational company wich sends **TO MODIFY**. Its main concurrent is the none so famous company **TO_MODIFY**.<br/>Founded in **DATE_OF_CREATION** in the dynamic city of **NAME_OF_CITY**, the society turnover is continually increasing since its creation. 
   
Actually, **REQUIREMENT_DEFINITION**.

# Project context
The provider will have to **PROJECT_DEFINITION**

# Requirement
The software must implement the following requirements and are organized in four categories:

* REQ_DESIGN_XXX: design requirements
* REQ_FUNC_XXX: functional requirements
* REQ_IHM_XXX: IHM requirements
* REQ_DATA_XXX: data managing requirements

## Design requirements
|||
|---:|:---|
|**REQ_DESIGN_001**|Test|
|**REQ_DESIGN_002**|Test|

## Functional requirements
|||
|---:|:---|
|**REQ_FUNC_001**|Test|
|**REQ_FUNC_002**|Test|

## IHM requirements
|||
|---:|:---|
|**REQ_IHM_001**|Test|
|**REQ_IHM_002**|Test|

## Data requirements
|||
|---:|:---|
|**REQ_DATA_001**|Test|
|**REQ_DATA_002**|Test|

# Delivery conditions
|||
|---:|:---|
|**Repository name**||
|**Repository right**|ramassage-tek|
|**Language**||
|**Platform target**||

The delivery package must contain the following documents:

## Documentary:

* SAS (Software Architecture Specifications). This document must follow the template "*Template - Software Architecture Specifications.docx*". It must contain an explanation of the provider understanding of the project, constraints and solutions proposed, global and detailed UML diagrams of the architecture proposition, and the state machine diagrams if needed.

 * SQS (Software Qualification Specifications). This document must follow the template "*Template - Software Qualifications Specifications.docx*". It must contain tests procedures to check the software. The documents must also contain a tracability matrix which link the requirement id with the test id. The provider should write a first version of this document only from this request for proposal and before any work.

 * SQSA (Software Qualification Specification Assurance). This document must follow the template "*Template - Software Qualifications Specifications Assurance.docx*". This is the SQS document filled with results of the tests procedures.

## Code and binairies
 * The software must be developed in **PROJECT_LANGUAGE** language. Classes and functions must be commented with Doxygen format. 

 * Binary executable on **PLATEFORM**

# Planning
   The planning of the project consists of several deadlines, T0 is the bebinning of the project:

|**Object**|**Week**|
|---:|:---|
|**Kick-off**      |T0|
|**Bootstrap**     |T0|
|**Follow up**     |T0 + 2 weeks|
|**Follow up**     |T0 + 4 weeks|
|**Delivery**      |T0 + 4 weeks|
|**Review** 	   |T0 + 5 weeks|

# Evolutions
   This is not mantadory, but the company appreciate if the provider you go further this project, adding functionalities which make the project more complex.

   Example: **EXAMPLES**, etc...