# Architecture
| **Details unit** ||
|-----------------------|---|
|Code unit |T-ARC-700|
|Name unit | Architecture |
|ECTS | 5 |
|Manager | nicolas1.moreel-lebon@epitech.eu |

|**Review**||
|:---|---|
|Activities|Updated|
|Description|Updated|
|Skills|Updated|
|Pedagogical info|Updated|

## Activities details
| **Activities** | **Type** | **Resum** | **Details** | **Delivery** | **Teachers time** | **Students time** |
|---------------|:---------:|---------------|---------------|---------------|-------------------|-------------------|
|Activity1|kick-off |||None|0.5|0.5|
|Activity|Lighting talk||- Groups of 2 people<br/>- Pros and cons of patterns describes in one of 6 subjects:<br/>1. MVC model, design factory, design decorator, design observator<br/>2. MVVM model, design builder, design composite, design strategy<br/>3. MVP model, design prototype, design adapter, design iterator<br/>4. MVC model, design singleton, design proxy, design template<br/>5. MVVM model, design abstract factory, design bridge, design callback<br/>6. MVP model, design factory, design facade, design interpreter|none||17.5|
|Activity|Project|Cash manager||Yes||49|
|Activity|Bootstrap|||None|3.0|3.0|
|Activity2|Pitch||Lighthing talk|None|2.0|0.5|
|Activity3|Follow Up|||None|2.0|0.5|
|Activity3|Follow Up|||None|2.0|0.5|
|Activity4|Review||Cash Manager|None|2.0|0.5|
||||||**11,5**|**72**|

## Unit Description
|Unit Description||
|:---|---|
|**Pedagogical goal**|Deviler a profesional documentation<br/>- SAS => Architecture<br/>- Technical documentation (Doxygen for example)|
|**Technical goal**|Design pattern and techno watch|

## Skills to be acquired
|Skills to be acquired|
|:---|
|Design pattern knowledge|

## Pedagogical information reserved for teachers
|Pedagogical information reserved for teachers|
|:---|
|[D] Lighting talk >=8<br/>[D] SAS >=5<br/>[C] SAS >= 7<br/>[C] Global covert >=50% \|\|<br/>functionnal >= 50% && Design >=75% && IHM >= 50%<br/>[C] lignthing talk >= 12<br/>[B] SAS >= 9<br/>[B] functionnal >= 50% && Design >=75% && IHM >= 50%<br/>[A] SAS + SQS + Technical Doc<br/>[A] functionnal >= 50% && Design >=75% && IHM >= 50%<br/>[A] Lighting talk >= 15|

## Planning
```mermaid
    gantt
    title Architecture unit: T-ARC-700
    dateFormat  YYYY-MM-DD

    section Kick-off            
    kick-off                    : 2018-12-03, 1w

    section Lighting talk
    Talk                        : done, talk, 2018-12-03, 1w

    section Project
    CashManager                 : done, cashmanager, 2018-12-10, 5w 
    
    section Bootstrap
    Bootstap                    : active, 2018-12-10, 1w

    section Follow up
    Follow up                   : active, crit, 2018-12-24, 1w
    Follow up                   : active, crit, 2019-01-07, 1w

    section Review
    Pitch                       : done, crit, after talk, 1w   
    Review                      : done, crit, after cashmanager, 1w
```