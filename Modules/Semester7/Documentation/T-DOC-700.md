# Unit name
| **Details unit** ||
|-----------------------|---|
|Code unit |T-DOC-700 |
|Name unit | Documentation |
|ECTS | 1 |
|Manager | nicolas1.moreel-lebon@epitech.eu |

|**Review**||
|:---|---|
|Activities|Updated|
|Description|Updated|
|Skills|Updated|
|Pedagogical info|Updated|

## Activities details
| **Activities** | **Type** | **Resum** | **Details** | **Delivery** | **Teachers time** | **Students time** |
|---------------|:---------:|---------------|---------------|---------------|-------------------|-------------------|
|Activity1|kick-off |||None|0.5|0.5|
|Activity2|Review|week 2 part 1|Technical doc (doxygen)|None|1.0|0.25|
|Activity3|Review|week 2 part 2|Technical doc (doxygen)|None|1.0|0.25|
|Activity4|Review|week 3|Technical doc (doxygen)|None|1.0|0.25|
|Activity6|Review|Job board|Complete doc<br/>(SAS + SQS + SQSA + doxygen) |None|1.0|0.25|
||||||**4,5**|**1,5**|

## Unit Description
|Unit Description|
|:---|
|Introduce professional documentation apply to web seminar.|

## Unit Description
|Unit Description|Discovery of documentation and process of<br/> a SSII|
|:---|---|
|**Pedagogical goal**|Deviler a profesional documentation<br/>- SAS => Architecture<br/>- SQS => Tests definition<br/>- SQSA => Tests result<br/>- Technical documentation (Doxygen for example)|
|**Technical goal**|Code documentation|

## Skills to be acquired
|Skills to be acquired|
|:---|
|Learn how make documentation<br/> - Technical (doxygen for example)<br/> - Architecture document (SAS)<br/> - Test definition document (SQS)<br/> - Test result document (SQSA)|

## Pedagogical information reserved for teachers
|Pedagogical information reserved for teachers|
|:---|
|First review (week 2) are using to explain how make technical documentation (doxygen format).<br/>For last week, students must make technical documentation for pool day and complete doc for last project (job board).<br/><br/> [D] Technical doc (doxygen) on several pool day.<br/> [C] Technical doc (doxygen) on job board.<br/>[C] Architecture documentation (SAS)<br/> [B] Test definition documentation (SQS)<br/>[A] Result test documentation (SQSA) apply to SQS|

## Planning
```mermaid
    gantt
    title C++ed unit: T-CPP-710
    dateFormat  YYYY-MM-DD

    section Kick-off            
    kick-off                        : 2018-10-17, 1w

    section Review   
    Review (Week2 part 1)           : done, crit, 2018-10-17, 1w
    Review (Week2 part 2)           : done, crit, 2018-10-17, 1w
    Review (Week3)                  : done, crit, 2018-10-25, 1w
    Review (Job Board)              : done, crit, 2018-10-25, 1w
```