# Mobile development
| **Details unit** ||
|-----------------------|---|
|Code unit |T-MOB-700|
|Name unit | Mobile development |
|ECTS | 3 |
|Manager | cecile.duvigneau@epitech.eu |

|**Review**||
|:---|---|
|Activities|Updated|
|Description|Updated|
|Skills|Updated|
|Pedagogical info|Updated|

## Activities details
| **Activities** | **Type** | **Resum** | **Details** | **Delivery** | **Teachers time** | **Students time** |
|---------------|:---------:|---------------|---------------|---------------|-------------------|-------------------|
|Activity1|kick-off |||None|0.5|0.5|
|Activity|Project|Ovalion||Yes||42|
|Activity2|Bootstrap|||None|3.0|3.0|
|Activity3|Workshop||Documentation|None|3.0|3.0|
|Activity4|Follow Up|||None|2.0|0.5|
|Activity5|Follow Up|||None|2.0|0.5|
|Activity6|Follow Up|||None|2.0|0.5|
|Activity7|Review|||None|2.0|0.5|
||||||**14,5**|**50,5**|

## Unit Description
|Unit Description||
|:---|---|
|**Pedagogical goal**|Deviler a profesional documentation<br/>- SAS => Architecture<br/>- SQS => Tests definition<br/>- SQSA => tests result<br/>- Technical documentation (Doxygen for example)|
|**Technical goal:**|- Discover Java Android development|

## Skills to be acquired
|Skills to be acquired|
|:---|
|Deliver technical documentations<br/>Make, follow and perform tests campain<br/>Discover Java Android development|

## Pedagogical information reserved for teachers
|Pedagogical information reserved for teachers|
|:---|
|[D] software architecture OK<br/>[D] Tests campain<br/>[C] Deliver a professional documentation<br/>[C] 50% of request complete<br/>[B] Follow and pass the tests campain<br/>[B] 75% of request complete<br/>[A] >=90% of request complete|

## Planning
```mermaid
    gantt
    title C++ unit: T-CPP-700
    dateFormat  YYYY-MM-DD

    section Kick-off            
    kick-off                    : 2018-10-01, 1w
    
    section Project
    Ovalion                     : done, ovalion, 2018-10-01, 7w 
    
    section Bootstrap
    Bootstap                    : active, 2018-10-01, 1w

    section Workshop
    Workshop doc                : active, 2018-10-08, 1w

    section Follow up
    Follow up                   : active, crit, 2018-10-15, 1w
    Follow up                   : active, crit, 2018-10-29, 1w
    Follow up                   : active, crit, 2018-11-12, 1w

    section Review   
    Review                      : done, crit, after ovalion, 1w
```