# Architecture
| **Details unit** ||
|-----------------------|---|
|Code unit |T-WIN-700|
|Name unit | Windows Development |
|ECTS | 3 |
|Manager | nicolas1.moreel-lebon@epitech.eu |

|**Review**||
|:---|---|
|Activities|Updated|
|Description|Updated|
|Skills|Updated|
|Pedagogical info|Updated|

## Activities details
| **Activities** | **Type** | **Resum** | **Details** | **Delivery** | **Teachers time** | **Students time** |
|---------------|:---------:|---------------|---------------|---------------|-------------------|-------------------|
|Activity1|kick-off|||None|0.5|0.5|
|Activity|Project|Epi Bubble||Yes||98|
|Activity2|Bootstrap|||None|3.0|3.0|
|Activity3|Follow Up|||None|2.0|0.5|
|Activity3|Follow Up|||None|2.0|0.5|
|Activity4|Review||Epi Bubble|None|2.0|0.5|
||||||**9,5**|**103**|

## Unit Description
|Unit Description||
|:---|---|
|**Pedagogical goal**|Deviler a profesional documentation<br/>- SAS => Architecture<br/>- SQS => Tests definition<br/>- SQSA => tests result<br/>- Technical documentation (Doxygen for example)|
|**Technical goal**|- Discover Windows development|

## Skills to be acquired
|Skills to be acquired|
|:---|
|Deliver technical documentations<br/>Make, follow and perform tests campain<br/>Discover Windows development<br/>Discover Windows environement|

## Pedagogical information reserved for teachers
|Pedagogical information reserved for teachers|
|:---|
|[D] software architecture OK<br/>[D] Tests campain<br/>[C] Deliver a professional documentation<br/>[C] 50% of request complete<br/>[B] Follow and pass the tests campain<br/>[B] 75% of request complete<br/>[A] >=90% of request complete|

## Planning
```mermaid
    gantt
    title Dev Windows unit: T-WIN-700
    dateFormat  YYYY-MM-DD

    section Kick-off            
    kick-off                    : 2018-12-17, 1w
    
    section Project
    EpiBubble                   : done, epibubble, 2018-12-17, 6w 

    section Bootstrap
    Bootstap 1                  : active, 2018-12-17, 1w

    section Follow up
    Follow up                   : active, crit, 2019-01-07, 1w
    Follow up                   : active, crit, 2019-01-21, 1w

    section Review   
    Review                      : done, crit, after epibubble, 1w
```