# C++ed
| **Details unit** ||
|-----------------------|---|
|Code unit |T-CPP-710|
|Name unit | C++ed |
|ECTS | 6 |
|Manager | nicolas1.moreel-lebon@epitech.eu|

|**Review**||
|:---|---|
|Activities|Updated|
|Description|Updated|
|Skills|Updated|
|Pedagogical info|Updated|

## Activities details
| **Activities** | **Type** | **Resum** | **Details** | **Delivery** | **Teachers time** | **Students time** |
|---------------|:---------:|---------------|---------------|---------------|-------------------|-------------------|
|Activity1|kick-off |||None|0.5|0.5|
|Activity|Project|AbstractVM||Yes||63|
|Activity2|Bootstrap||Retour sur le modern CPP|None|3.0|3.0|
|Activity3|Follow Up|||None|2.0|0.5|
|Activity4|Review|||None|2.0|0.5|
|Activity1|kick-off |||None|0.5|0.5|
|Activity|Project|Plazza||Yes||126|
|Activity2|Bootstrap||Présentation de:<br/>- Thread<br/>- Mutex<br/>- Cond var|None|3.0|3.0|
|Activity2|Bootstrap||Présentation de:<br/>- Process (fork, wait, exec)<br/>- mkfifo|None|3.0|3.0|
|Activity3|Follow Up|||None|2.0|0.5|
|Activity3|Follow Up|||None|2.0|0.5|
|Activity4|Review|||None|2.0|0.5|
||||||**20,5**|**201,5**|

## Unit Description
|Unit Description||
|:---|---|
|**Pedagogical goal**|Deviler a profesional documentation<br/>- SAS => Architecture<br/>- SQS => Tests definition<br/>- SQSA => tests result<br/>- Technical documentation|
|**Technical goal**|- Modern CPP (C++14)<br/>- Good software architecture<br/>- Process (fork, wait, exec)<br/>- MKFIFO<br/>- Thread<br/>- Mutex & Cond var|

## Skills to be acquired
|Skills to be acquired|
|:---|
|Modern CPP<br/>Deliver technical documentations<br/>Make, follow and pass tests campain|

## Pedagogical information reserved for teachers
|Pedagogical information reserved for teachers|
|:---|
|[D] software architecture OK<br/>[D] Tests campain<br/>[C] Deliver a professional documentation<br/>[C] 50% of request complete<br/>[B] Follow and pass the tests campain<br/>[B] 75% of request complete<br/>[A] >=90% of request complete|

## Planning
```mermaid
    gantt
    title C++ed unit: T-CPP-710
    dateFormat  YYYY-MM-DD

    section Kick-off            
    kick-off (Abstract)         : 2018-10-22, 1w
    kick-off (Plazza)           : 2018-11-05, 1w
    
    section Project
    AbstractVm                  : done, abstractvm, 2018-10-22, 2w 
    Plazza                      : done, plazza, 2018-11-05, 4w
    
    section Bootstrap
    Bootstrap (Abstract)        : active, 2018-10-22, 1w
    Bootstap (Thread)           : active, 2018-11-05, 1w
    Bootstap (process)          : active, 2018-11-05, 1w

    section Follow up
    Follow up (Abstract)        : active, crit, 2018-10-29, 1w
    Follow up 1 (Plazza)        : active, crit, 2018-11-12, 1w
    Follow up 2 (Plazza)        : active, crit, 2018-11-26, 1w
    
    section Review   
    Review (Abstract)           : done, crit, after abstractvm, 1w
    Review (plazza)             : done, crit, after plazza, 1w
```