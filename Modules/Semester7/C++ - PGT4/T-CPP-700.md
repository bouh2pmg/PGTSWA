# C++
| **Details unit** ||
|-----------------------|---|
|Code unit |T-CPP-700|
|Name unit | C++ |
|ECTS | 4 |
|Manager | nicolas1.moreel-lebon@epitech.eu |

|**Review**||
|:---|---|
|Activities|Updated|
|Description|Updated|
|Skills|Updated|
|Pedagogical info|Updated|

## Activities details
| **Activities** | **Type** | **Resum** | **Details** | **Delivery** | **Teachers time** | **Students time** |
|---------------|:---------:|---------------|---------------|---------------|-------------------|-------------------|
|Activity1|kick-off |||None|0.5|0.5|
|Activity|Project|Battle Space||Yes||77|
|Activity2|Bootstrap|||None|3.0|3.0|
|Activity3|Follow Up|||None|2.0|0.5|
|Activity3|Follow Up|||None|2.0|0.5|
|Activity3|Follow Up|||None|2.0|0.5|
|Activity4|Review|||None|2.0|0.5|
||||||**11,5**|**82,5**|

## Unit Description
|Unit Description|Discovery of documentation and process of<br/> a SSII|
|:---|---|
|**Pedagogical goal**|Deviler a profesional documentation<br/>- SAS => Architecture<br/>- SQS => Tests definition<br/>- SQSA => tests result<br/>- Technical documentation (Doxygen for example)|
|**Technical goal**|- Modern CPP (C++14)<br/>- Good software architecture<br/>- Game protocol|

## Skills to be acquired
|Skills to be acquired|
|:---|
|Modern CPP<br/>Deliver technical documentations<br/>Make, follow and perform tests campain<br/>Make a good game protocol and its documentation|

## Pedagogical information reserved for teachers
|Pedagogical information reserved for teachers|
|:---|
|[D] software architecture OK<br/>[D] Tests campain<br/>[C] Deliver a professional documentation<br/>[C] Make a game protocol and its documentation<br/>[C] 50% of request complete<br/>[B] Follow and pass the tests campain<br/>[B] 75% of request complete<br/>[A] >=90% of request complete|

## Planning
```mermaid
    gantt
    title C++ unit: T-CPP-700
    dateFormat  YYYY-MM-DD

    section Kick-off            
    kick-off                    : 2018-10-22, 1w
    
    section Project
    BattleSpace                 : done, battlespace, 2018-10-22, 6w 
    
    section Bootstrap
    Bootstap                    : active, 2018-10-22, 1w

    section Follow up
    Follow up                   : active, crit, 2018-11-05, 1w
    Follow up                   : active, crit, 2018-11-12, 1w
    Follow up                   : active, crit, 2018-11-26, 1w

    section Review   
    Review                      : done, crit, after battlespace, 1w
```