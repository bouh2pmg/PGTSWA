# Plannings

```mermaid
    gantt
    title PGTSWA: Semester 7
    dateFormat  YYYY-MM-DD
    #dateFormat  YYYY-WW


    section PGT4
    Dev Mobile   : active, 2018-10-01, 7w    
    C++          : active, 2018-10-29, 6w

    section PGT4(ed) Common
    Architecture            : done, 2018-12-03, 6w
    Dev Windows             : done, 2018-12-17, 6w

    section PGT4ed
    Web Seminar          : crit, active, 2018-09-10, 3w
    Documentation        : crit, active, 2018-09-17, 2w
    Piscine C            : crit, active, 2018-10-01, 3w
    C++(ed)              : crit, active, 2018-10-22, 6w
```

```mermaid
    gantt
    title PGTSWA: Semester 8
    dateFormat  YYYY-MM-DD

    section PGT4
    Embedded                    : active, 2019-02-04, 7w
    Data base                   : active, 2019-03-04, 5w
    Web Architecture            : active, 2019-04-01, 7w
    Dev Mobile, cross platform  : active, 2019-05-13, 6w
    Artificial Intelligence     : active, 2019-06-03, 7w
    Spec Functionnal            : done, 2019-03-04, 24w
```