# Plannings

```mermaid
    gantt
    title PGTSWA: Semester 9
    dateFormat  YYYY-MM-DD

    section PGT5
    Project Management I        : active, 2018-09-10, 22w
    Quality Assurance I         : active, 2018-09-27, 22w
    Continuous Integration I    : active, 2018-09-24, 20w
    Development I               : active, 2018-09-24, 20w
    Cloud                       : active, 2018-10-08, 10w
    English                     : active, 2018-10-08, 18w
    Part-Time                   : active, 2018-09-03, 23w
```

```mermaid
    gantt
    title PGTSWA: Semester 10
    dateFormat  YYYY-MM-DD

    section PGT5
    Project Management II       : active, 2019-02-11, 33w
    Quality Assurance II        : active, 2019-02-11, 33w
    Continuous Integration II   : active, 2019-02-11, 33w
    Development II              : active, 2019-02-11, 33w
    Advanced Cloud              : active, 2019-02-11, 29w
    English                     : active, 2019-02-11, 20w
    Part-Time                   : active, 2019-02-11, 33w
```