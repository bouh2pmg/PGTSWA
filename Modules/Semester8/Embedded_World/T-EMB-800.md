# Unit name
| **Details unit** ||
|-----------------------|---|
|Code unit |T-EMB-800 |
|Name unit | Embedded |
|ECTS | 4 |
|Manager | cecile.duvigneau@epitech.eu |

|**Review**||
|:---|---|
|Activities|Not Updated|
|Description|Not Updated|
|Skills|Not Updated|
|Pedagogical info|Not Updated|

## Activities details
| **Activities** | **Type** | **Resum** | **Details** | **Delivery** | **Teachers time** | **Students time** |
|---------------|:---------:|---------------|---------------|---------------|-------------------|-------------------|
|Activity1|kick-off |||None|0.5|0.5|
|Activity|Project|||Yes||119|
|Activity2|Bootstrap|||None|3.0|3.0|
|Activity3|Follow Up|||None|2.0|0.5|
|Activity4|Review|||None|2.0|0.5|
||||||**7.5**|**123.5**|

## Unit Description
|Unit Description||
|:---|---|
|**Pedagogical goal**|Goblablablablablablablablablaod|
|**Technical goal**|Goblablablablablablablablablaod|

## Skills to be acquired
|Skills to be acquired|
|:---|
|Blablablalbalablalblaalalblabllalalbalalalblalablba|

## Pedagogical information reserved for teachers
|Pedagogical information reserved for teachers|
|:---|
|Blablablalbalablalblaalalblabllalalbalalalblalablba|

## Planning
```mermaid
    gantt
    title xxx unit: T-XXX-XXX
    dateFormat  YYYY-MM-DD

    section Kick-off            
    kick-off                    : 2018-10-01, 1w
    
    section Project
    ProjectX                    : done, abstractvm, 2018-10-01, 2w 
    
    section Lighting talk
    Talk1                       : done, 2018-10-01, 1w

    section Bootstrap
    Bootstap 1                  : active, 2018-10-01, 1w

    section Workshop
    workshop1                   : active, 2018-10-01, 1w

    section Follow up
    Follow up                   : active, crit, 2018-10-08, 1w

    section Review   
    Review                      : done, crit, 2018-10-15, 1w
```
