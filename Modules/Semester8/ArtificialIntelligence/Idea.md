# Thèmes à aborder
## Perceptron simple
### Bootstrap & porte logique

## Réseaux de neurone
### my_ocr : reconnaissance digit (images)
Reconnaissance MNIST

### Reconnaissance de digit (led)
7 entrées, 10 sorties, 1 ou 2 couches cachée

## CNN (convultionnal Neural Network) & Framework
### Egle Eyes : reconnaissance de pattern
Base NORB <br/>
Base CIFAR

### Reconnaissance audio

## liquid state machine
### Lighting talk

## Génétique + Reseaux ...

#Lien utiles : 
https://ujjwalkarn.me/2016/08/11/intuitive-explanation-convnets/
https://ujjwalkarn.me/2016/08/09/quick-intro-neural-networks/
http://www.wildml.com/2015/11/understanding-convolutional-neural-networks-for-nlp/
https://arxiv.org/pdf/1512.03385.pdf
  
http://yann.lecun.com/exdb/mnist/
http://data.dmlc.ml/

http://www2.ift.ulaval.ca/~lamontagne/ift17587/modules/module3/r%C3%A9seauxNeurones.pdf
https://fr.wikipedia.org/wiki/Fonction_d%27activation
http://www.emergentmind.com/

https://www.google.com/search?client=firefox-b-ab&biw=1536&bih=750&ei=JncOW9fCGYbkUZr0hJgL&q=NORB+dataset&oq=NORB+dataset&gs_l=psy-ab.3..0j0i22i30k1l5.2878171.2878171.0.2878914.1.1.0.0.0.0.88.88.1.1.0....0...1c.1.64.psy-ab..0.1.88....0.BusItsXQByY

https://cs.nyu.edu/~ylclab/data/norb-v1.0/
http://yann.lecun.com/exdb/publis/
https://cs.nyu.edu/~yann/research/norb/index.html
http://www.ee.surrey.ac.uk/CVSSP/demos/chars74k/
