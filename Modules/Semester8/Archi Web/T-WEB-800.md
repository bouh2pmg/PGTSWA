# Unit name
| **Details unit** ||
|-----------------------|---|
|Code unit |T-WEB-800 |
|Name unit | Web Architecture |
|ECTS | 4 |
|Manager | nicolas1.moreel-lebon@epitech.eu |

|**Review**||
|:---|---|
|Activities|Updated|
|Description|Updated|
|Skills|Updated|
|Pedagogical info|Not Updated|

## Activities details
| **Activities** | **Type** | **Resum** | **Details** | **Delivery** | **Teachers time** | **Students time** |
|---------------|:---------:|---------------|---------------|---------------|-------------------|-------------------|
|Activity|kick-off |||None|0.5|0.5|
|Activity|Project|ScreenFleet||Yes||168|
|Activity|Bootstrap|||None|3.0|3.0|
|Activity|Follow Up|||None|2.0|0.5|
|Activity|Follow Up|||None|2.0|0.5|
|Activity|Follow Up|||None|2.0|0.5|
|Activity|Review||Screen fleet|None|2.0|0.5|
||||||**11,5**|**173,5**|

## Unit Description
|Unit Description||
|:---|---|
|**Pedagogical goal**|Mettre en place une architecture web complète,<br/>utilisant des micro services|
|**Technical goal**|Découverte des framework web. Choix technique en fonction des besoins.|

## Skills to be acquired
|Skills to be acquired|
|:---|
| - Architecture web<br/> - Framework web<br/> - Micro services|

## Pedagogical information reserved for teachers
|Pedagogical information reserved for teachers|
|:---|
||

## Planning
```mermaid
    gantt
    title Web Architecture unit: T-WEB-800
    dateFormat  YYYY-MM-DD

    section Kick-off            
    kick-off                    : 2019-04-01, 1w
    
    section Project
    ScreenFleet                 : done, ScreenFleet, 2019-04-01, 7w 

    section Bootstrap
    Bootstap 1                  : active, 2019-04-01, 1w

    section Follow up
    Follow up                   : active, crit, 2019-04-15, 1w
    Follow up                   : active, crit, 2019-04-29, 1w
    Follow up                   : active, crit, 2019-05-13, 1w

    section Review   
    Review                      : done, crit, after ScreenFleet, 1w
```
