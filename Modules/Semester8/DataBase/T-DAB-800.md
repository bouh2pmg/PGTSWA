# Unit name
| **Details unit** ||
|-----------------------|---|
|Code unit |T-DAB-800 |
|Name unit | DataBase |
|ECTS | 3 |
|Manager | nicolas1.moreel-lebon@epitech.eu |

|**Review**||
|:---|---|
|Activities|Updated|
|Description|Updated|
|Skills|Updated|
|Pedagogical info|Updated|

## Activities details
| **Activities** | **Type** | **Resum** | **Details** | **Delivery** | **Teachers time** | **Students time** |
|---------------|:---------:|---------------|---------------|---------------|-------------------|-------------------|
|Activity|Kick-off|Module + lighting talk||None|0.5|0.5|
|Activity|Lighting talk||Faire une présentation sur une technologie SQL et/ou NoSQL.<br/> - MySQL vs MongoBD<br/> - PostgreSQL vs Neo4j<br/> - mariaDB vs orientDB ou infiniteDB<br/> - Technos NoSQL (Documents, clé-valeurs, colonnes, graph)<br/>(http://www.lemagit.fr/article/NoSQL-le-choix-difficile-de-la-bonne-technologie) |None||15|
|Activity|Pitch||Lighting talk|None|2.0|0.5|
|Activity|Kick-off|Projet||None|0.5|0.5|
|Activity|Project|The Square||Yes||70|
|Activity|Bootstrap|||None|3.0|3.0|
|Activity|Follow Up|||None|2.0|0.5|
|Activity|Review|||None|2.0|0.5|
||||||**10**|**90,5**|

## Unit Description
|Unit Description||
|:---|---|
|**Pedagogical goal**|Veille techno sur les technologies de base de données.<br/>|
|**Technical goal**| - Découvrir les différences entre les technologie SQL et NoSQL.<br/> - Créer une base de données en choisissant la/les meilleurs<br/>technologie et justifier ces choix.|

## Skills to be acquired
|Skills to be acquired|
|:---|
| - Connaissance base de données SQL.<br/> - Connaissance base de données NoSQL.<br/> - Création d'une base de données à destination d'un réseau social professionnel.|

## Pedagogical information reserved for teachers
|Pedagogical information reserved for teachers|
|:---|
|[D] software architecture OK<br/>[D] Tests campain<br/>[C] Deliver a professional documentation<br/>[C] Lighting talk >= 8<br/>[C] 50% of request complete<br/>[B] Follow and pass the tests campain<br/>[B] 75% of request complete<br/>[B] Lighting talk >= 14<br/>[A] >=90% of request complete<br/>[A] Lighting talk >= 16|

## Planning
```mermaid
    gantt
    title DataBase unit: T-DAB-800
    dateFormat  YYYY-MM-DD

    section Kick-off            
    kick-off                    : 2019-03-04, 1w
    kick-off                    : 2019-03-11, 1w
    
    section Project
    TheSquare                   : done, TheSquare, 2019-03-11, 4w 
    
    section Lighting talk
    Talk1                       : done, Talk1, 2019-03-04, 1w

    section Bootstrap
    Bootstap 1                  : active, 2019-03-11, 1w

    section Follow up
    Follow up                   : active, crit, 2019-03-25, 1w

    section Pitch
    Pitch                       : done, crit, after Talk1, 1w

    section Review   
    Review                      : done, crit, after TheSquare, 1w
```
