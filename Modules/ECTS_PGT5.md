# Count of ECTS for PGT5

## PGT5 unit
|**Semester**   |**PGT5 unit**              |**ECTS**|
|-----------    |-----------                |------- |
|S9             |Project Management I       |5       |
|S9             |Quality Assurance I        |3       |
|S9             |Continuous Integration I   |5       |
|S9             |Development I              |5       |
|S9             |Cloud                      |3       |
|S9             |English                    |2       |
|S10            |Project Management II      |5       |
|S10            |Quality Assurance II       |3       |
|S10            |Continuous Integration II  |5       |
|S10            |Development II             |5       |
|S10            |Advanced Cloud             |6       |
|S10            |English                    |2       |
|S9&10          |Part-time                  |35      |
|**S9&10**      |**Sum**                    |**84**  |
