# Count of ECTS for PGT4

## PGT4 unit
|**Semester**   |**PGT4 unit**  |**ECTS**|
|-----------    |-----------    |------- |
|S7             |C++            |4       |
|S7             |Dev Mobile     |3       |
|S8             |Part time      |35      |
|**S7&8**       |**Sum**        |**42**  |

## PGT4ed unit
|**Semester**   |**PGT4ed unit**|**ECTS**   |
|-----------    |-----------    |-------    |
|S7             |Web seminar    |4          |
|S7             |Documentation  |1          |
|S7             |C Pool         |3          |
|S7             |C++ed          |6          |
|S8             |Part time      |30         |
|**S7&8**       |**Sum**        |**44**     |

## Common unit
|**Semester**|**Common unit**            |**ECTS**|
|----------- |---------------------------|------- |
|S7          |Architecture               |5       |
|S7          |Dev Windows                |3       |
|S8          |Embedded                   |4       |
|S8          |Data Base                  |3       |
|S8          |Dev Mobile cross platform  |3       |
|S8          |Web Architecture           |4       |
|S8          |Artificial Inteligence     |4       |
|S8          |Fonctionnal Specification  |4       |
|**S7&8**    |**Sum**                    |**30**  |